window.onload = __setup;
function __setup() {


    var fd = new FormData;
    fd.append("op", "load_chart_data");
    //fetch the chart data
    ajax("/", parseJsonDataForChart, fd);
}


function ajax(url, callback, data) {
    try {
        var x = new (this.XMLHttpRequest || ActiveXObject)('MSXML2.XMLHTTP.3.0');
        x.open(data ? 'POST' : 'GET', url, 1);
        x.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        x.onreadystatechange = function () {
            x.readyState > 3 && callback && callback(x.responseText);
        };
        x.send(data);
    } catch (e) {
        window.console && console.log(e);
    }
}



function parseJsonDataForChart(e) {

    var json = JSON.parse(e);
    var result_parsed = [];
    var current_date;

    var date_ranges = [];

    parseDatesAndPercentagesIntoWeeklyGroups(json, date_ranges); //function toparse the date columns intoweekly groups of 7days




    setupChart(date_ranges);



}

function parseDatesAndPercentagesIntoWeeklyGroups(json, date_range_array) {

    var date_range_index = 0;
    //group onboarding dates in weekly ranges
    for (var i = 0; i <= json.length; i++) {
        var current_date = json[i].created_at; //get the first date detected

        var d = Date.parse(current_date);
        var seven_dayz_ends_at = d + (6 * 86400000); //current day+6days


        //if the remaining dates would alsways be less than seven days from the current 
        //we just grab the remaining date ranges then exit
        if (Date.parse(json[json.length - 1].created_at) <= seven_dayz_ends_at) {

            //create an object for this 7 day data-set
            date_range_array[date_range_index] = {};
            date_range_array[date_range_index].data = [];

            //store the percentages for the days that fallinto the current 7 day range
            for (var k = i; k < json.length; k++) {
                date_range_array[date_range_index].data.push(parseInt(json[k].onboarding_percentage))

            }
            date_range_array[date_range_index++].name = json[i].created_at + " - " + json[k - 1].created_at


            i = json.length;
            break;
        }


        for (var j = 0; j < json.length; j++) {


            //if the next date is within the cuurent_date +6
            if ((Date.parse(json[j].created_at) <= seven_dayz_ends_at)) {



            } else {

                //create an object for this 7 day data-set
                date_range_array[date_range_index] = {};
                date_range_array[date_range_index].data = [];

                //store the percentages for the days that fallinto the current 7 day range
                for (var k = i; k < j; k++) {
                    date_range_array[date_range_index].data.push(parseInt(json[k].onboarding_percentage));


                }
                date_range_array[date_range_index++].name = json[i].created_at + " - " + json[j - 1].created_at


                break;
            }

        }
        i = j;


    }
}


function setupChart(seriesData) {
    console.log(seriesData);
    var chartOptions = {
   chart: {
        },
        title: {
            text: 'Onboarding process completion indicator'
        },

        subtitle: {
            text: 'assessment'
        },
        xAxis: {
            categories: ['1', '2', '3', '4', '5', '6', '7', '8'],
            tickInterval: 1,
            title: {
                text: 'Onboarding stage'
            },
            labels: {
                enabled: true
            }

        },
        yAxis: {
            labels: {
                formatter: function () {
                    return this.value + ' %';
                }
            },

            title: {
                text: 'Onboarding Percentage'
            }
        },
    
        
        series: seriesData,
        responsive: {
            rules: [{
                    condition: {
                        maxWidth: 500
                    },
                    chartOptions: {
                        legend: {
                            layout: 'horizontal',
                            align: 'center',
                            verticalAlign: 'bottom'
                        }
                    }
                }]
        }

    };
    Highcharts.chart('appContainer', chartOptions);
}