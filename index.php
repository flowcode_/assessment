<?php

function csvtojson($file, $delimiter) {//load and json encode a csv
    if (($handle = fopen($file, "r")) === false) {
       // die("can't open the file.");
    }

    $csv_headers = fgetcsv($handle, 4000, $delimiter);
    $csv_json = array();

    while ($row = fgetcsv($handle, 4000, $delimiter)) {
        $csv_json[] = array_combine($csv_headers, $row);
    }

    fclose($handle);
    return json_encode($csv_json);
}

if ($_SERVER['REQUEST_METHOD'] === "POST" && isset($_POST['op']) && $_POST['op'] === "load_chart_data") {


    echo csvtojson("./App/data/export.csv", ",");
    exit;
}
?>

<!DOCTYPE html>
<head> 


    <script src="lib/HighChart/highcharts.js"></script>
    <script src="lib/HighChart/modules/series-label.js"></script>

    <script src="js/index.js"></script>
    <link rel="stylesheet" href="css/index.css" />
</head>

<body>
    <div class='header' >

    </div>
    <div class='appContainer' id='appContainer'>


    </div>



</body>

